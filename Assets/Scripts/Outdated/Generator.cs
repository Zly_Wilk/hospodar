﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour {

	public GameObject hexPrefab;

	static readonly float WIDTH_MULTIPLIER = Mathf.Sqrt(3) / 2;
	float radius = 0.5f;
	int width = 32;
	int height = 24;

	float xOffset = Mathf.Sqrt(3) / 2;
	float zOffset = 0.75f;
	// Use this for initialization
	void Start () {
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {

				float xPos = x * xOffset;
				// Are we on an odd row?
				if( y % 2 == 1 ) {
					xPos += xOffset/2f;
				}

				GameObject hex_go = (GameObject)Instantiate(hexPrefab, new Vector3( xPos,0, y * zOffset  ), Quaternion.identity  );

				// Name the gameobject something sensible.
				hex_go.name = "Hex_" + x + "_" + y;

				// Make sure the hex is aware of its place on the map
				//hex_go.GetComponent<Hex>().x = x;
				//hex_go.GetComponent<Hex>().y = y;

				// For a cleaner hierachy, parent this hex to the map
				//hex_go.transform.SetParent(this.transform);

				// TODO: Quill needs to explain different optimization later...
				hex_go.isStatic = true;

			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
