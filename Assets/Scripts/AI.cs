﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AI  {

	int influence=0;
	int countryForce;
	private string name;
	Region[] regions;

	Flag ally= new Flag(false);
	Flag overlord= new Flag(false);
	Flag trade= new Flag(false);
	Flag war= new Flag(false);

	public bool hasTurn= false;

	public AI(){
	}

	public AI(string name, int initialCountryForce){
		this.name = name;
		this.countryForce = initialCountryForce;
	}

	public bool move(Gamer gamer, GameObject warDeclaration){
	
		if (this.countryForce > gamer.getCountryForce ()) {
			declareWar (warDeclaration);
		}
		hasTurn = true;
		return hasTurn;
	}

	public void declareWar(GameObject warDeclaration){
		warDeclaration.SetActive (true);
		warDeclaration.GetComponentsInChildren<Text>()[1].text = getName () + " wypowiada nam wojnę!";
	}

	public string getName(){

		return name;
	}

	public bool offerDeffensiveAlliance(){
		Console.WriteLine("To dziala dla " + name);
		return false;
	}

	public bool offerTrade(){
		return false;
	}

	public int getInfluence(){
		return 0;
	}
}
