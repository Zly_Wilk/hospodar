﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiplomacyHandler : MonoBehaviour
{
	public int aiNr;
	public Text influence;

	AI bot;
    // Start is called before the first frame update
    void Start()    {
		
		bot = Handler.Instance.bots [aiNr]; 
		
		if(bot.GetType().Name=="AIZew"){
			influence = GetComponent<Text>();
		}else{
			Destroy(influence);
		}
		
    }

    // Update is called once per frame
    void Update()    {
		if(bot.GetType().Name=="AIZew"){
			influence.text = "Wpływy: "+Handler.Instance.bots [aiNr].getInfluence().ToString ();
		}
    }

	public void offerAlliance(){

		bool answer;
		Handler.Instance.bots [aiNr].offerDeffensiveAlliance ();
	}
	
}
