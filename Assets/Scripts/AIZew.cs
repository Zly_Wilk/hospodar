﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AIZew : AI {

	public int influence;

	public AIZew() : base(){
	}

	public AIZew(string name, int initialCountryForce) : base(name, initialCountryForce){
	}
	
	public int getInfluence(){
		return influence;
	}
}
