﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gamer {

	int nation;
	//0- Wallahia
	//1- Moldavia
	//2- Transylvania

	[System.NonSerialized] public int cash=0;
	[System.NonSerialized] public int armyForce=0;
	[System.NonSerialized] public int countryForce;

	[System.NonSerialized] public int HabInfluence;
	[System.NonSerialized] public int PlcInfluence;
	[System.NonSerialized] public int RusInfluence;
	[System.NonSerialized] public int TurInfluence;
	//0-100

	public int PeasantRebel;
	public int NobleRebel;
	//0-100

	public int armyRequitmentPolice;
	public int armyMaintenancePolice;
	public int peasantTaxPolice;
	public int nobleTaxPolice;
	int income;

	[System.NonSerialized] public Flag independence= new Flag(true);
	[System.NonSerialized] public Flag tribute= new Flag(false);
	[System.NonSerialized] public Flag lawsRecentlyUpdated= new Flag(false);
	[System.NonSerialized] public Flag technologyRecentlyUpdated= new Flag(false);
	[System.NonSerialized] public Flag armyRecentlyUpdated= new Flag(false); 
	[System.NonSerialized] public Flag oppositionExecuted= new Flag(false);
	[System.NonSerialized] public Flag oppositionExiled= new Flag(false);
	[System.NonSerialized] public Flag fightTheRebels= new Flag(false);
	[System.NonSerialized] public Flag fightTheWar= new Flag(false);
	//flagi

	[System.NonSerialized] public int lawUpdatesCounter=0;
	[System.NonSerialized] public int technologyUpdatesCounter=0;
	[System.NonSerialized] public int armyUpdatesCounter=0;

	[System.NonSerialized] public static int size= 8;
	public Region[] regions= new Region[size];
	int capital;

	public Gamer(int nation){
	
		this.nation = nation;

		if (nation == 0) {
			this.regions [0] = Handler.Instance.regions [0];
			this.regions [1] = Handler.Instance.regions [1];
			capital = 0;
		} else if (nation == 1) {
			this.regions [2] = Handler.Instance.regions [2];
			this.regions [3] = Handler.Instance.regions [3];
			capital = 2;
		} else {
			this.regions [4] = Handler.Instance.regions [4];
			this.regions [5] = Handler.Instance.regions [5];
			capital = 4;
		}
	}


	public void rebelsProgress(){

		PeasantRebel += peasantTaxPolice;
		NobleRebel += nobleTaxPolice;

		if (fightTheRebels.getFlag ()) {
			PeasantRebel -= 100;
			NobleRebel -= 100;
		}

		if (lawsRecentlyUpdated.getFlag ()) {
			PeasantRebel -= 50;
			NobleRebel -= 50;
		}

		if (fightTheRebels.getFlag () && fightTheRebels.getEndOfFlag () == 0) {
			int province = 0;

			while (!Handler.Instance.regions [province].occupiedByRebels.getFlag()) {
				province = Random.Range (0, 8);
			}

			this.regions [province] = Handler.Instance.regions [province];
			fightTheRebels = new Flag (Handler.Instance.getCurrentTurn (), 5);
		}

		if (PeasantRebel >= 1000 && numberOfProvinces()>1){// && gamer.getCountryForce() < 0) {
			SpawnPeasantRebel ();
			fightTheRebels = new Flag (true);
		}else if(NobleRebel >= 1000 && numberOfProvinces()>1){ //&& gamer.getCountryForce() < 0) {
			SpawnNobleRebel ();
			fightTheRebels = new Flag (true);
		}



	}


	public void SpawnPeasantRebel(){

		int province= 0;
		PeasantRebel = 0;

		while (regions [province] != null && province != capital) {
			province = Random.Range (0, 8);
		}

		regions [province].occupiedByRebels = new Flag (true);
		regions [province] = null;
	}	

	public void SpawnNobleRebel(){

		int province= 0;
		NobleRebel= 0;

		while (regions [province] != null && province != capital) {
			province = Random.Range (0, 8);
		}

		regions [province].occupiedByRebels = new Flag (true);
		regions [province] = null;
	}	

	public int getIncome(){

		int income;

		if (technologyRecentlyUpdated.getFlag ()) {
			income = 	((nobleTaxPolice + peasantTaxPolice + getTradeIncome () - armyMaintenancePolice - 50) * technologyUpdatesCounter / 10) +
				nobleTaxPolice + peasantTaxPolice + getTradeIncome () - armyMaintenancePolice - 50;
		} else {
			income = 	((nobleTaxPolice + peasantTaxPolice + getTradeIncome () - armyMaintenancePolice) * technologyUpdatesCounter / 10) +
				nobleTaxPolice + peasantTaxPolice + getTradeIncome () - armyMaintenancePolice;
		}

		income*=numberOfProvinces();

		if (tribute.getFlag()&&income>0)
			income /= 2;



		return income;
	} 

	public int getCountryForce(){
	
		return  getArmyForce() + cash;
	}

	public int getTradeIncome(){
	
		return 0;	
	}

	public int getArmyForce(){
	

		if (armyRecentlyUpdated.getFlag ()) {
			
			armyForce=	(int)(((armyMaintenancePolice + armyRequitmentPolice-50)*armyUpdatesCounter)/10)+
							armyMaintenancePolice + armyRequitmentPolice-50;
		}else{
				
			armyForce=	(int)(((armyMaintenancePolice + armyRequitmentPolice) * armyUpdatesCounter) / 10) + armyMaintenancePolice +
							armyRequitmentPolice;	
		}

		armyForce *= numberOfProvinces ();

		return armyForce;
	}



	private int numberOfProvinces(){
		int counter = 0;

		for (int i = 0; i < size; i++) {
			if (regions [i] != null)
				counter++;
		}
		return counter;
	}
		
}
