﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EventSystem : MonoBehaviour
{


	private Text eventTitle;
	private Text eventContent;


	// Start is called before the first frame update
    void Start()
    {
		eventTitle = GetComponentsInChildren<Text>()[0];
		eventContent = GetComponentsInChildren<Text>()[1];
    }

    // Update is called once per frame
    void Update()
    {
		modifyEventTexts(Handler.Instance.getCurrentTurn ());
    }

	private void modifyEventTexts(int turn){
		if (turn == 1) {
			
			eventTitle.text = "Początek";
			eventContent.text = "\tNo i się zaczęło. \n1 test \n2 test \n3 test";
		} else if (turn == 20) {
		} else if (turn == 166) {
		
			eventTitle.text = "Koniec";
			eventContent.text = "Tym razem nie pykło.";
		}
	}
}
