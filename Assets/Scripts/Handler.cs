﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Handler  : Singleton<Handler> {

	public int country;


	public Slider sliderNobleTax;
	public Slider sliderPeasantTax;
	public Slider sliderARequitment;
	public Slider sliderAMaintenance;

	public GameObject warDeclarationPanel;
	public GameObject eventPanel;

	public Gamer gamer;

	public AI[] bots= new AI[6];
	[System.NonSerialized] public AI currentBot;

	public Region[] regions= new Region[8];

	public int turns;

	// Use this for initialization
	void Start () {


		regions [0] = new Region ("Targovitse");
		regions [1] = new Region ("Oltenia");
		regions [2] = new Region ("Jassy");
		regions [3] = new Region ("Kiszyniów");
		regions [4] = new Region ("Kluż");
		regions [5] = new Region ("Alam");
		regions [6] = new Region ("Białogród");
		regions [7] = new Region ("Konstanca");
		//lista regionów

		gamer= new Gamer (country);
		//0- Wallahia
		//1- Moldavia
		//2- Transylvania


		AIZew Hab= new AIZew("Austria", 1);
		AIZew Plc= new AIZew("Polska-Litwa", 1);
		AIZew Rus= new AIZew("Rosja", 1);
		AIZew Tur= new AIZew("Turcja", 1);

		bots [0] = Hab;
		bots [1] = Plc;
		bots [2] = Rus;
		bots [3] = Tur;

		if (country == 0) {
			AINrod Mol= new AINrod("Mołdawia", 1);
			AINrod Tra= new AINrod("Siedmiogród", 1);
			bots [4] = Mol;
			bots [5] = Tra;
		} else if (country == 1) {
			AINrod Wal= new AINrod("Wołoszczyzna", 1);
			AINrod Tra= new AINrod("Siedmiogród", 1);
			bots [4] = Wal;
			bots [5] = Tra;
		} else {
			AINrod Wal= new AINrod("Wołoszczyzna", 1);
			AINrod Mol= new AINrod("Mołdawia", 1);
			bots [4] = Wal;
			bots [5] = Mol;
		}
		//lista botów

		turns = 0;

	}
	
	// Update is called once per frame
	void Update () {

		if (gamer.HabInfluence < 0) {
			gamer.HabInfluence = 0;
		}
		if (gamer.PlcInfluence < 0) {
			gamer.PlcInfluence = 0;
		}
		if (gamer.RusInfluence < 0) {
			gamer.RusInfluence = 0;
		}
		if (gamer.TurInfluence < 0) {
			gamer.TurInfluence = 0;
		}
		if (gamer.armyForce < 0) {
			gamer.armyForce = 0;
		}
		if (gamer.countryForce < 0) {
			gamer.countryForce = 0;
		}
		if (gamer.cash < 0) {
			gamer.cash = 0;
		}
		if (gamer.PeasantRebel < 0) {
			gamer.PeasantRebel = 0;
		}
		if (gamer.NobleRebel < 0) {
			gamer.NobleRebel = 0;
		}
		//dolna granica statystyk gracza

		if (gamer.HabInfluence > 100) {
			gamer.HabInfluence = 100;
		}
		if (gamer.PlcInfluence > 100) {
			gamer.PlcInfluence = 100;
		}
		if (gamer.RusInfluence > 100) {
			gamer.RusInfluence = 100;
		}
		if (gamer.TurInfluence > 100) {
			gamer.TurInfluence = 100;
		}
		//górna granica statystyk gracza

		setArmyRequitmentPolice ();
		setArmyMaintenancePolice ();
		setPeasantTaxPolice ();
		setNobleTaxPolice ();
	}

	public void nextTurn(){


		for (int i = 0; i < 6; i++)
			bots [i].hasTurn=false;
		
		for (int i = 0; i < 6; i++) {
			currentBot = bots [i];
			bots [i].move (gamer, warDeclarationPanel);
		}
		//tura botow

		gamer.cash += gamer.getIncome ();

		gamer.rebelsProgress ();




		turns++;

		gamer.lawsRecentlyUpdated.clearFlag (getCurrentTurn());
		gamer.technologyRecentlyUpdated.clearFlag (getCurrentTurn());
		gamer.armyRecentlyUpdated.clearFlag (getCurrentTurn());
		gamer.fightTheRebels.clearFlag (getCurrentTurn());


		spawnEvent (turns);//or don't spawn, i don't know
	}

	int spawnEvent(int turn){
	
		if(turn == -1){
			eventPanel.SetActive (true);
			return 0;//event ID
		}return 0;
	}

	public int getCurrentTurn(){
		return turns;
	}

	//poczatek suwaczkow
	public void setArmyRequitmentPolice(){
		
		gamer.armyRequitmentPolice= (int) sliderARequitment.value;
	}

	public void setArmyMaintenancePolice(){

		gamer.armyMaintenancePolice= (int) sliderAMaintenance.value;
	}

	public void setPeasantTaxPolice(){

		gamer.peasantTaxPolice= (int) sliderPeasantTax.value;
	}

	public void setNobleTaxPolice(){

		gamer.nobleTaxPolice= (int) sliderNobleTax.value;
	}
	//koniec suwaczkow

	public void updateLaws(){
		if (gamer.cash >= 50) {
			gamer.cash -= 50;
			gamer.lawsRecentlyUpdated = new Flag (getCurrentTurn (), 5);
			gamer.lawUpdatesCounter++;
		}
	}

	public void updateTechnology(){
		if (gamer.cash >= 50) {
			gamer.cash -= 50;
			gamer.technologyRecentlyUpdated = new Flag (getCurrentTurn (), 5);
			gamer.technologyUpdatesCounter++;
		}
	}

	public void updateArmy(){
		if (gamer.cash >= 50) {
			gamer.cash -= 50;
			gamer.armyRecentlyUpdated = new Flag (getCurrentTurn (), 5);
			gamer.armyUpdatesCounter++;
		}
	}

	public void executeOpposition(){
		if (gamer.cash >= 50) {
			gamer.cash -= 50;
			gamer.oppositionExecuted = new Flag (getCurrentTurn (), 5);
		}
	}

	public void exileOpposition(){
		if (gamer.cash >= 50) {
			gamer.cash -= 50;
			gamer.oppositionExiled = new Flag (getCurrentTurn (), 5);
		}
	}
}
