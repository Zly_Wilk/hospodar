﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CashToText : MonoBehaviour
{
	private Text currentCash;
    // Start is called before the first frame update
    void Start()
    {
		currentCash = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
		currentCash.text = Handler.Instance.gamer.cash.ToString ();
    }
}
