﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdministracyPanel : MonoBehaviour
{
	public GameObject lawButton;
	public GameObject executionButton;
	public GameObject exileButton;
	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

		lawButton.SetActive (!Handler.Instance.gamer.lawsRecentlyUpdated.getFlag ());
		executionButton.SetActive (!Handler.Instance.gamer.oppositionExecuted.getFlag ());
		exileButton.SetActive (!Handler.Instance.gamer.oppositionExiled.getFlag ());
	}

	public void updateLaw(){
		Handler.Instance.updateLaws ();
	}

	public void executeOpposition(){
		Handler.Instance.executeOpposition ();
	}

	public void exileOpposition(){
		Handler.Instance.exileOpposition ();
	}

}
