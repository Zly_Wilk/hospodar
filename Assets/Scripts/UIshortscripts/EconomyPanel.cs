﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EconomyPanel : MonoBehaviour
{
	public GameObject technologyButton;
	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

		technologyButton.SetActive (!Handler.Instance.gamer.technologyRecentlyUpdated.getFlag ());


	}

	public void updateTechnology(){
		Handler.Instance.updateTechnology ();
	}
}
