﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderToText : MonoBehaviour
{

	public Slider sliderUI;
	private Text textSliderValue;

    // Start is called before the first frame update
    void Start()
    {
		textSliderValue = GetComponent<Text>();
		ShowSliderValue();
	}
	public void ShowSliderValue () {
		string sliderMessage = sliderUI.value.ToString();
		textSliderValue.text = sliderMessage;
	} 
    

    // Update is called once per frame
    void Update()
    {
		ShowSliderValue();
    }
}
