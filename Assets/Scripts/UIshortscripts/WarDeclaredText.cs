﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WarDeclaredText : MonoBehaviour
{
	private Text warDeclared;
	// Start is called before the first frame update
	void Start()
	{
		warDeclared = GetComponent<Text>();
	}

	// Update is called once per frame
	void Update()
	{
		if(Handler.Instance.currentBot.hasTurn)
		warDeclared.text = Handler.Instance.currentBot.getName () + " wypowiada nam wojnę!";
	}
}
