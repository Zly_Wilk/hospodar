﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArmyForceToText : MonoBehaviour
{
	private Text currentArmyForce;
    // Start is called before the first frame update
    void Start()
    {
		currentArmyForce = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
		currentArmyForce.text = Handler.Instance.gamer.getArmyForce().ToString ();
    }
}
