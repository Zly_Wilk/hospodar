﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IncomeToText : MonoBehaviour
{
	private Text income;//todo: wyswietlaj na ekranie
	//public Handler handler;
	// Start is called before the first frame update
	void Start()
	{
		income = GetComponent<Text>();
	}

	// Update is called once per frame
	void Update()
	{
		income.text = Handler.Instance.gamer.getIncome().ToString();
	}
}
