﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnsCounter : MonoBehaviour
{
	private Text currentTurn;//todo: wyswietlaj na ekranie
	//public Handler handler;
    // Start is called before the first frame update
    void Start()
    {
		currentTurn = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
		currentTurn.text = (1548+2*Handler.Instance.getCurrentTurn()).ToString();
    }
}
