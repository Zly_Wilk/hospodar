﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PeasantRToText : MonoBehaviour
{
	private Text currentRebelsSize;
	// Start is called before the first frame update
	void Start()
	{
		currentRebelsSize = GetComponent<Text>();
	}

	// Update is called once per frame
	void Update()
	{
		currentRebelsSize.text = "Peasant rebels size= " + Handler.Instance.gamer.PeasantRebel.ToString ();
	}
}
