﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flag {
    
	private bool isSet;
	private int startTurn;
	private int duration;
	private int endTurn;

	public Flag(bool isSet){
		this.isSet = isSet;
	}

	public Flag(int startTurn, int duration){
		this.isSet = true;
		this.startTurn = startTurn;
		this.duration = duration;
		this.endTurn = startTurn + duration;
	}

	public void setFlag(bool isSet){
		this.isSet = isSet;
	}

	public bool getFlag(){
		return isSet;
	}

	public int getEndOfFlag(){
		return endTurn;
	}

	public void clearFlag(int currentTurn){
	
		if (endTurn == currentTurn) {
		
			setFlag (false);
			endTurn = 0;
		}
	}
}
